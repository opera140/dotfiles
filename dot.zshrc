export WORKON_HOME=/home/ccousin/.virtualenvs

# load zgen
source "/home/ccousin/.zgen/zgen.zsh"

# if the init scipt doesn't exist
if ! zgen saved; then
    echo "Creating a zgen save"

    zgen oh-my-zsh

    # plugins
    zgen oh-my-zsh plugins/git
    zgen oh-my-zsh plugins/sudo
    zgen oh-my-zsh plugins/virtualenvwrapper
    zgen oh-my-zsh plugins/command-not-found

    zgen load zsh-users/zsh-syntax-highlighting
    zgen load Tarrasch/zsh-autoenv

    # completions
    zgen load zsh-users/zsh-completions src

    # theme
    zgen oh-my-zsh themes/amuse

    # save all to init script
    zgen save
fi

# Misc
export PAGER="less"
export EDITOR="vim"
export PYTHONSTARTUP=~/.pythonrc

source ~/Tools/git-subrepo/.rc

alias reload='. ~/.zshrc'
alias v='/usr/bin/vim'

function vim() {
    gvim $1 &
}

