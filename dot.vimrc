" Opera's .vimrc
" 2016/10/18
" version 2.0

" General configuration
set nocompatible        " Disable vi compatibility
set number              " Show line numbers
set rnu                 " Show relative line numbers
set numberwidth=5       " Line numbers on 5 columns
set backspace=indent    " Allow backspace over indentation
set backspace+=eol      " end of lines
set backspace+=start    " and line begin
set wildmenu            " Menubar
set showmatch           " Show matching braces
set matchpairs+=<:>     " <> matches too
set mouse=a             " Enable mouse
set showmode            " Show current vim mode
set showcmd             " Show commands
set nobackup            " No backup (~) files
set noswapfile          " No swap (.swp) files
set history=100         " Command line history size
set cursorline          " Highlight cursor line
set scrolloff=5         " 5 line buffer when scrolling
set laststatus=2        " Needed by airline
set foldmethod=syntax   " Folding

" Search
set incsearch           " Incremental search
set nohlsearch          " Search doesn't highlight
set ignorecase          " Case insensitive search
set smartcase           " Ignore case when searching lowercase

" Indentation
set autoindent          " Keep indentation for new lines
set tabstop=4           " <Tab> character width
set smarttab            " Enables shiftwidth
set shiftwidth=4        " <Tab> character width if it's the first of the line

" Color theme
colorscheme ir_black

" GVim
set guioptions-=m  " Remove menu bar
set guioptions-=T  " Remove toolbar
set guioptions-=r  " Remove right-hand scroll bar
set guioptions-=L  " Remove left-hand scroll bar

" Full screen
if has("gui_running")
  set lines=999 columns=999
 endif

" Syntax hightlighting
if has("syntax")
    syntax on
endif

" Highlight spaces at the end of lines
highlight WhitespaceEOL ctermbg=red guibg=red
let m = matchadd("WhitespaceEOL", "\\s\\+$", 2)
autocmd BufEnter * match WhitespaceEOL /\s+$/

" Some key mappimg
nnoremap <C-s> :w<CR>
nnoremap <silent> <C-t>] :tabnext<CR>
nnoremap <silent> <C-t>[ :tabprevious<CR>
nnoremap <silent> ] <C-w>l
nnoremap <silent> [ <C-w>h
nnoremap <silent> - <C-w>j
nnoremap <silent> = <C-w>k
imap jj <Esc>


let g:clang_library_path = '/usr/lib/llvm-3.8/lib'

" Vundle

filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Plugins
" Nerdtree + git plugin
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'Xuyuanp/nerdtree-git-plugin'

" Syntax and completion
Plugin 'scrooloose/syntastic'
Plugin 'Valloric/YouCompleteMe'
Plugin 'rdnetto/YCM-Generator'
Plugin 'octol/vim-cpp-enhanced-highlight'

" Vim fun
Plugin 'ervandew/supertab'
Plugin 'vim-airline/vim-airline'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'vim-airline/vim-airline-themes'

" Git
Plugin 'tpope/vim-fugitive'
Plugin 'airblade/vim-gitgutter'

" Classic vim
Plugin 'vim-scripts/a.vim'
Plugin 'vim-scripts/taglist.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Airline
let g:airline_theme='base16'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_tabs = 1

" YCM
let g:ycm_confirm_extra_conf = 0
autocmd CompleteDone * pclose " Close completion preview

" Plugins key mapping
" NERDTree
nnoremap <C-d> :NERDTreeToggle<CR>
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ "Unknown"   : "?"
    \ }

" A
" Switch header/src
" Header/src vsplit
" Open file under cursor
" Open file under cursor vsplit
nnoremap <F2> :AT<CR>
nnoremap <F3> :A<CR>
nnoremap <F4> :AV<CR>
nnoremap <F5> :IHT<CR>
nnoremap <F6> :IHV<CR>

" CtrlP
nnoremap <C-p> :CtrlP<CR>

